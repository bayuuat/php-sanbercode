<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LATIHAN SOAL 3</title>
</head>
<body>
    <?php
        echo "<h2> SOAL 1 </h2>";
        function tentukan_nilai($number)
        {
            if($number >= 85 && $number <= 100){
                return "Sangat Baik <br>";
            } 
            else if($number >= 70 && $number < 85) {
                return "Baik <br>";
            }
            else if($number >= 60 && $number < 70) {
                return "Cukup <br>";
            }
            else {
                return "Kurang <br>";
            }
        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang

        echo "<h2> SOAL 2 </h2>";
        function ubah_huruf($string){
            for ($i = 0; $i <= (strlen($string) - 1); $i++) {
                $word  = $string[$i];
                echo ++$word;
            }
            echo "<br>";
        }
            
        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu

        echo "<h2> SOAL 3 </h2>";
        function tukar_besar_kecil($string){
            for ($i = 0; $i <= (strlen($string) - 1); $i++) {
                $word  = $string[$i];
                if(ctype_upper($word)){
                    $word = strtolower($word);
                } else if(ctype_lower($word)){
                    $word = strtoupper($word);
                }
                echo $word;
            } 
            echo "<br>";
        }
        
        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
            
    ?>
</body>
</html>