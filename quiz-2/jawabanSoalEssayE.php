SELECT customers.name AS customr_name, SUM(orders.amount) AS total_amount 
FROM orders 
INNER JOIN customers ON orders.customer_id = customers.id
GROUP BY customers.name
ORDER BY SUM(orders.amount) ASC