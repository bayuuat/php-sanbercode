CREATE TABLE customers(
    id int AUTO_INCREMENT,
    name varchar (255),
    email varchar (255),
    password varchar (255),
    PRIMARY KEY(id)
);

CREATE TABLE orders(
    id int AUTO_INCREMENT,
    amount varchar (255),
    customer_id int,
    PRIMARY KEY(id)
);

ALTER TABLE orders
ADD FOREIGN KEY (customer_id) REFERENCES customers(id)