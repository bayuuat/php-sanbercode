<?php
    require ('animal.php');

    class Ape extends Animal{
        public function yell(){
            echo "Auooo";
        }
    }
    
    class Frog extends Animal{
        public $legs = 4;
        public function jump(){
            echo "hop hop";
        }
    }

    $sungokong = new Ape("kera sakti");
    echo "Nama hewan = " .$sungokong->name. "<br>" ; // "shaun"
    echo "Jumlah kaki = " .$sungokong->legs. "<br>"; // 2
    echo "Darah dingin = ";
    echo var_dump($sungokong->cold_blooded). "<br>"; // false
    $sungokong->yell(); // "Auooo"
    echo "<br><br>";
    
    $kodok = new Frog("buduk");
    echo "Nama hewan = " .$kodok->name. "<br>" ; // "shaun"
    echo "Jumlah kaki = " .$kodok->legs. "<br>"; // 2
    echo "Darah dingin = ";
    echo var_dump($kodok->cold_blooded). "<br>"; // false
    $kodok->jump(); // "hop hop"
    echo "<br><br>";
?>