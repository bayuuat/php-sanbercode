<?php
    class Animal{
        public $name;
        public $legs = 2;
        public $cold_blooded = false;

        public function __construct($name){
            $this->name = $name;
        }
    }

    $sheep = new Animal("shaun");

    echo "Nama hewan = " .$sheep->name. "<br>" ; // "shaun"
    echo "Jumlah kaki = " .$sheep->legs. "<br>"; // 2
    echo "Darah dingin = ";
    echo var_dump($sheep->cold_blooded). "<br>"; // false
    echo "<br><br>";
?>
